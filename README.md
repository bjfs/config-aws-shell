# README #

Bart's sample configuration for AWS CloudShell

### What is this repository for? ###

* Basic stuff which makes navigating in AWS more interesting, like adding nano and switching to the Z shell. It defaults to Canada for ..reasons..
* Version 0.2

### How do I get set up? ###

Clone this repo directly into the $HOME of spawned AWS CloudShell, should work after restart; if not then type "activate" ;-)
