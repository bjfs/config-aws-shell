# .bashrc

RED='\033[1;31m'
NC='\033[0m' # No Color

activate(){
    printf "Bootstraping CloudShell:\n"
    printf "  * ${RED}Ensuring nano existence${NC}\n\n"
    which nano || sudo yum install -y nano
    printf "\n  * ${RED}Switching to the Z shell${NC}\n\n"
    exec zsh
}

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
complete -C '/usr/local/bin/aws_completer' aws
export AWS_EXECUTION_ENV=CloudShell
activate
