#
# .zshrc is sourced in interactive shells.
# It should contain commands to set up aliases,
# functions, options, key bindings, etc.
#


autoload -U compinit; compinit

autoload promptinit; promptinit
prompt fire red magenta blue white white white

autoload colors; colors

#allow tab completion in the middle of a word
setopt COMPLETE_IN_WORD

## keep background processes at full speed
#setopt NOBGNICE
## restart running processes on exit
#setopt HUP

## history
#setopt APPEND_HISTORY
## for sharing history between zsh processes
#setopt INC_APPEND_HISTORY
#setopt SHARE_HISTORY

## never ever beep ever
#setopt NO_BEEP

## automatically decide when to page a list of completions
#LISTMAX=0

## disable mail checking
#MAILCHECK=0

# autoload -U colors
#colors
autoload -Uz compinit && compinit
autoload bashcompinit && bashcompinit
complete -C '/usr/local/bin/aws_completer' aws

echo "executed $ZSH_NAME version $ZSH_VERSION"
echo 
echo "  * $fg[green]Switching to the Central region (Canada)$reset_color"
export AWS_REGION="ca-central-1"
echo
echo region $AWS_REGION is now default
echo
echo " * $fg[green]Get psyched!$reset_color"
echo
echo "Your session JSON is:"
aws sts get-caller-identity|jq
echo
echo "Prompt:"
